#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import random


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            if not line:
                break
            print("El cliente nos manda " + line.decode('utf-8'))
            if line.decode('utf-8').split(' ')[0] == 'INVITE':
                parte2 = "\r\n\r\nv=0\r\no=batman@gotham.com 127.0.0.1\r\n" \
                         "s=misesion\r\nt=0\r\nm=audio 67876 RTP"
                parte1 = "SIP/2.0 100 Trying\r\n\r\n" \
                         + "SIP/2.0 180 Ringing\r\n\r\n" \
                         + "SIP/2.0 200 OK\r\nContent-Length:" \
                         + str(len(parte2))
                parte3 = parte1 + parte2
                self.wfile.write(bytes(parte3, "utf-8"))
            elif line.decode('utf-8').split(' ')[0] == 'ACK':
                ALEAT = random.randint(1, 11111)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(pad_flag=0, ext_flag=0,
                                      cc=0, marker=0, ssrc=ALEAT)
                audio = simplertp.RtpPayloadMp3(audio_file)
                simplertp.send_rtp_packet(RTP_header, audio,
                                          "127.0.0.1", 23032)
            elif line.decode('utf-8').split(' ')[0] == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")

        # Si no hay más líneas salimos del bucle infinito


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    audio_file = sys.argv[3]
    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
